// Package pservice is a Proxy Service API utility
package pservice

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"path"
	"strconv"
	"sync"
	"time"
)

// Default pool size if a length paramater is not specified
const poolSize = 1000

// Proxy returned by the service
type Proxy struct {
	ID  uint   `json:"id"`
	URL string `json:"url"`
}

// PService allow you to interact with the Proxy Service API
// to use a pool of proxies
//
// ctx, cancel := context.WithCancel(context.Background())
// defer cancel()
//
// ps := pservice.New(apiURL, apiKey, target)
// err := ps.Start(ctx)
type PService struct {
	url      string
	apiKey   string
	target   string
	pool     []Proxy
	excluded sync.Map
	Proxies  chan *Proxy
	Blocked  chan *Proxy
	client   *http.Client
}

type responseData struct {
	Pool []Proxy `json:"pool"`
}

type apiResponse struct {
	Data responseData `json:"data"`
}

// New create and return a new PService instance
func New(url string, APIKey string, target string) *PService {
	ps := &PService{
		url:    url,
		apiKey: APIKey,
		target: target}
	timeout := time.Duration(30 * time.Second)
	ps.client = &http.Client{
		Timeout: timeout,
	}
	return ps
}

// FetchProxies fetch a new list of proxies for the target
func (ps *PService) FetchProxies(query map[string]string) error {
	url, err := url.Parse(ps.url)
	url.Path = path.Join(url.Path, "proxy_list")
	url.Path = path.Join(url.Path, ps.target)
	req, err := http.NewRequest("GET", url.String(), nil)
	if err != nil {
		return err
	}

	q := req.URL.Query()
	q.Set("api_key", ps.apiKey)
	for param, value := range query {
		q.Set(param, value)
	}
	l := q.Get("len")
	if l == "" {
		q.Set("len", strconv.Itoa(poolSize))
	}
	req.URL.RawQuery = q.Encode()

	resp, err := ps.client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	if resp.StatusCode != 200 {
		return fmt.Errorf("No 200 response found: %s", body)
	}

	// log.Println(string(body))
	response := &apiResponse{}
	err = json.Unmarshal(body, response)
	if err != nil {
		return err
	}

	ps.pool = response.Data.Pool

	return nil
}

// ProcessBlocked check for new blocked proxies
func (ps *PService) ProcessBlocked(ctx context.Context) {
	for {
		select {
		case <-ctx.Done():
			return
		case p := <-ps.Blocked:
			ps.excluded.Store(p.ID, true)
		}
	}
}

// Cycle send proxies to the pool until a Stop signal is received
func (ps *PService) Cycle(ctx context.Context) {
	poolLenth := len(ps.pool)
	ps.Proxies = make(chan *Proxy, poolLenth)
	defer close(ps.Proxies)
	current := 0
	bll := poolLenth
	for {
		select {
		case <-ctx.Done():
			log.Printf("PROXY SERVICE: canceling - reason: %+v\n", ctx.Err())
			return
		default:
		}
		if len(ps.pool) == 0 {
			log.Println("PROXY SERVICE: error found => Proxy Pool is empty")
			return
		}
		if current >= len(ps.pool) {
			current = 0
		}
		proxy := &ps.pool[current]
		if _, ok := ps.excluded.Load(proxy.ID); ok && bll > 0 {
			log.Println("PROXY SERVICE: Ignoring Blocked: ", proxy.ID)
			bll--
			current++
			continue
		}
		if bll == 0 {
			log.Println("PROXY SERVICE: Max blocked limit reached")
			ps.excluded.Delete(proxy.ID)
		}
		bll = poolLenth
		select {
		case <-ctx.Done():
			log.Printf("PROXY SERVICE: canceling - reason: %+v\n", ctx.Err())
			return
		case ps.Proxies <- proxy:
			current++
		}
	}
}

// Start initialize the goroutines and make the proxies available
func (ps *PService) Start(ctx context.Context, query map[string]string) error {
	err := ps.FetchProxies(query)
	if err != nil {
		return err
	}
	go ps.ProcessBlocked(ctx)
	go ps.Cycle(ctx)

	return err
}
