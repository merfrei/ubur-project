// Package importer contains different types of importer for the crawlers
package importer

import (
	"context"
	"log"
	"sync"
)

// Importer is the base interface to implement for all the importers
type Importer interface {
	ImportItems(context.Context, *sync.WaitGroup, <-chan interface{}) <-chan interface{}
}

func printItems(ctx context.Context, wg *sync.WaitGroup, items <-chan interface{}) {
	defer wg.Done()
	ix := 0
	for {
		select {
		case <-ctx.Done():
			log.Printf("PRINT ITEMS: canceling - reason: %+v\n", ctx.Err())
			return
		case item, ok := <-items:
			if !ok {
				log.Println("PRINT ITEMS: all items printed")
				return
			}
			ix++
			log.Println(ix, " - Scraped: ", item)
		}
	}
}

// Pipeline take an importer, import the items and display them to stdout
func Pipeline(ctx context.Context, wg *sync.WaitGroup, imp Importer, results <-chan interface{}) {
	// Importer
	items := imp.ImportItems(ctx, wg, results)
	// Printer
	wg.Add(1)
	go printItems(ctx, wg, items)
}
