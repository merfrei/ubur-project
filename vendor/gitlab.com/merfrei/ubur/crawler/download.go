package crawler

import (
	"bytes"
	"context"
	"log"
	"net/http"
	"net/url"
	"sync"
	"time"

	"github.com/pkg/errors"
)

func makeRequest(r *Request) (*http.Response, error) {
	var err error
	var client *http.Client
	var proxyURL *url.URL
	var req *http.Request

	if r.ProxyService != nil {
		ctx, cancel := context.WithTimeout(context.Background(), time.Second*1)
		defer cancel()
		select {
		case <-ctx.Done():
			log.Printf("DOWNLOADER: Unable to get proxy for %+v\n", r)
		case proxy, ok := <-r.ProxyService.Proxies:
			if !ok {
				log.Println("DOWNLOADER: PS error, closed pool")
			} else {
				r.Proxy = proxy.URL
			}
		}
	}
	if r.Proxy != "" {
		proxyURL, err = url.Parse(r.Proxy)
		if err != nil {
			return nil, err
		}
		client = &http.Client{
			Transport: &http.Transport{
				Proxy: http.ProxyURL(proxyURL)},
			Timeout: r.Timeout}
	} else {
		client = &http.Client{Timeout: r.Timeout}
	}

	if r.IsForm {
		r.Body = bytes.NewBufferString(r.FormData.Encode())
	}

	req, err = http.NewRequest(r.Method, r.URL, r.Body)
	if err != nil {
		return nil, err
	}

	if r.IsForm {
		req.Header.Set("Content-Type", "application/x-www-form-urlencoded; param=value")
	}

	for hk, hv := range r.Header {
		req.Header.Set(hk, hv)
	}

	for ck, cv := range r.Cookies {
		req.AddCookie(&http.Cookie{Name: ck, Value: cv})
	}

	return client.Do(req)
}

// Download process a requests stream and return a new responses stream
func Download(ctx context.Context, wg *sync.WaitGroup,
	requests <-chan *Request, desc string) <-chan *http.Response {
	responsesStream := make(chan *http.Response)
	wg.Add(1)
	go func() {
		defer wg.Done()
		defer close(responsesStream)
		var rwg sync.WaitGroup
		for {
			select {
			case <-ctx.Done():
				log.Printf("DOWNLOADER [%s]: canceling - reason: %v\n", desc, ctx.Err())
				goto end
			case r, ok := <-requests:
				if !ok {
					log.Printf("DOWNLOADER [%s]: done\n", desc)
					goto end
				}
				rwg.Add(1)
				go func() {
					defer rwg.Done()
					resp, err := makeRequest(r)
					if err != nil {
						log.Printf("DOWNLOADER [%s]: error found => %+v",
							desc, errors.Wrap(err, "Downloader Error"))
						return
					}
					select {
					case <-ctx.Done():
						log.Printf("DOWNLOADER [%s]: canceling - reason: %v\n", desc, ctx.Err())
						return
					case responsesStream <- resp:
					}
				}()
			}
		}
	end:
		rwg.Wait()
	}()
	return responsesStream
}
