package crawler

import (
	"context"
	"io"
	"log"
	"net/url"
	"sync"
	"time"

	"gitlab.com/merfrei/ubur/pkg/pservice"
)

// Request represents a new HTML request to be done
type Request struct {
	Method       string
	URL          string
	Header       map[string]string
	Cookies      map[string]string
	Proxy        string
	ProxyService *pservice.PService
	Timeout      time.Duration
	Body         io.Reader
	FormData     url.Values
	IsForm       bool
	Meta         map[string]interface{}
}

// RequestsFactory generates new requests from a list of URLs
// it allows to setup some shared setups between them
type RequestsFactory struct {
	Method       string
	Header       map[string]string
	Cookies      map[string]string
	Proxy        string
	ProxyService *pservice.PService
	Timeout      time.Duration
	Body         io.Reader
}

// RequestsFromStream parse a stream of URLs and generate a stream of requests
func (rf *RequestsFactory) RequestsFromStream(
	ctx context.Context, wg *sync.WaitGroup,
	urls <-chan *url.URL, desc string) <-chan *Request {
	requests := make(chan *Request)
	wg.Add(1)
	go func() {
		defer wg.Done()
		defer close(requests)
		for {
			select {
			case <-ctx.Done():
				log.Printf("REQUESTS [%s]: canceling - reason: %v\n", desc, ctx.Err())
				return
			case url, ok := <-urls:
				if !ok {
					log.Printf("REQUESTS [%s]: done\n", desc)
					return
				}
				newRequest := &Request{
					Method:       rf.Method,
					URL:          url.String(),
					Header:       rf.Header,
					Cookies:      rf.Cookies,
					Proxy:        rf.Proxy,
					ProxyService: rf.ProxyService,
					Timeout:      rf.Timeout,
					Body:         rf.Body,
				}
				select {
				case <-ctx.Done():
					log.Printf("REQUESTS [%s]: canceling - reason: %v\n", desc, ctx.Err())
					return
				case requests <- newRequest:
				}
			}
		}
	}()
	return requests
}

// RequestsFromSlide parse a slide of URLs and generate a stream of requests
func (rf *RequestsFactory) RequestsFromSlide(ctx context.Context, desc string, urls ...*url.URL) <-chan *Request {
	requests := make(chan *Request)
	go func() {
		defer close(requests)
		for _, url := range urls {
			select {
			case <-ctx.Done():
				log.Printf("REQUESTS [%s]: canceling - reason: %v\n", desc, ctx.Err())
				return
			default:
			}
			newRequest := &Request{
				Method:       rf.Method,
				URL:          url.String(),
				Header:       rf.Header,
				Cookies:      rf.Cookies,
				Proxy:        rf.Proxy,
				ProxyService: rf.ProxyService,
				Timeout:      rf.Timeout,
				Body:         rf.Body,
			}
			select {
			case <-ctx.Done():
				log.Printf("REQUESTS [%s]: canceling - reason: %v\n", desc, ctx.Err())
				return
			case requests <- newRequest:
			}
		}
	}()
	return requests
}
