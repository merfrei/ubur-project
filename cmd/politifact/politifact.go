package main

import (
	"context"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"regexp"
	"strings"
	"sync"
	"syscall"
	"time"

	"github.com/PuerkitoBio/goquery"
	"github.com/pkg/errors"
	"gitlab.com/merfrei/ubur-project/items"
	"gitlab.com/merfrei/ubur-project/pkg/selectors"
	"gitlab.com/merfrei/ubur/crawler"
	"gitlab.com/merfrei/ubur/pipelines/importer"
	"gitlab.com/merfrei/ubur/pkg/config"
	mongoM "gitlab.com/merfrei/ubur/pkg/mongo"
)

const indexURL = "https://www.politifact.com/factchecks/"
const userAgent = "Mozilla/5.0 (X11; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0"

var headers = map[string]string{
	"User-Agent": userAgent,
}

var timeout = time.Duration(time.Second * 30)

type articleURL *url.URL
type nextPageURL *url.URL

// parseIndex is the parse method for the main page
// it extracts and add the article and page URLs to results
func parseIndex(ctx context.Context, r *http.Response) <-chan interface{} {
	resultS := make(chan interface{})
	go func() {
		defer close(resultS)
		defer r.Body.Close()
		doc, err := goquery.NewDocumentFromReader(r.Body)
		if err != nil {
			log.Printf("PARSER [INDEX]: error found => %+v\n", errors.Wrap(err, "Parser Error"))
			return
		}
		doc.Find(".m-statement__quote a[href]").Each(
			func(_ int, s *goquery.Selection) {
				link, exists := s.Attr("href")
				if !exists {
					err = fmt.Errorf("Link does not have attribute href: %v", s)
					log.Printf("PARSER [INDEX]: error found => %+v", errors.Wrap(err, "Parser Error"))
					return
				}
				linkURL, err := url.Parse(link)
				if err != nil {
					log.Printf("PARSER [INDEX]: error found => %+v\n", errors.Wrap(err, "Parser Error"))
					return
				}
				select {
				case <-ctx.Done():
					return
				case resultS <- articleURL(r.Request.URL.ResolveReference(linkURL)):
				}
			})
		doc.Find("a.c-button.c-button--hollow:contains(Next)").Each(
			func(_ int, p *goquery.Selection) {
				link, exists := p.Attr("href")
				if !exists {
					err = fmt.Errorf("Next Page does not have attribute href: %v", p)
					log.Printf("PARSER [INDEX]: error found => %+v", errors.Wrap(err, "Parser Error"))
					return
				}
				linkURL, err := url.Parse(link)
				if err != nil {
					log.Printf("PARSER [INDEX]: error found => %+v\n", errors.Wrap(err, "Parser Error"))
					return
				}
				select {
				case <-ctx.Done():
					return
				case resultS <- nextPageURL(r.Request.URL.ResolveReference(linkURL)):
				}
			})
	}()
	return resultS
}

// parseArticle parse the article data from the article page
// add a new Article item to results
func parseArticle(ctx context.Context, r *http.Response) <-chan interface{} {
	resultS := make(chan interface{})
	go func() {
		defer close(resultS)
		defer r.Body.Close()
		sdre := regexp.MustCompile(`\s(\w+\s\d+,\s\d+)\s`)
		doc, err := goquery.NewDocumentFromReader(r.Body)
		if err != nil {
			log.Printf("PARSER [ARTICLE]: error found => %+v\n", errors.Wrap(err, "Parser Error"))
			return
		}
		doc.Find("main").Each(
			func(_ int, s *goquery.Selection) {
				url := r.Request.URL.String()
				splits := strings.Split(url, "/")
				var identifier string
				for i := len(splits) - 1; i >= 0; i-- {
					if splits[i] != "" {
						identifier = splits[i]
						break
					}
				}
				article := items.Article{
					Identifier:    identifier,
					URL:           url,
					Title:         s.Find("h2.c-title").Text(),
					Author:        s.Find(".m-author__content > a").Text(),
					PublishedDate: s.Find(".m-author__date").Text(),
					Claim:         strings.TrimSpace(s.Find(".m-statement__quote-wrap > .m-statement__quote").First().Text()),
					Rating:        selectors.FindAttr(s, ".m-statement__meter .c-image__original", "alt"),
					Tags:          []string{},
					Sources:       []string{},
				}
				dateStr := s.Find(".m-statement__desc").Text()
				matches := sdre.FindStringSubmatch(dateStr)
				if len(matches) > 1 {
					article.ClaimDate = matches[1]
				}
				s.Find("ul.m-list.m-list--horizontal a.c-tag").Each(func(_ int, a *goquery.Selection) {
					article.Tags = append(article.Tags, a.Find("span").Text())
				})
				s.Find("#sources article p").Each(func(_ int, p *goquery.Selection) {
					article.Sources = append(article.Sources, p.Text())
				})

				select {
				case <-ctx.Done():
					return
				case resultS <- &article:
				}
			})
	}()
	return resultS
}

func indexCrawl(ctx context.Context, c *sync.WaitGroup,
	requests <-chan *crawler.Request) <-chan interface{} {
	indexResponses := crawler.Download(ctx, c, requests, "INDEX")
	return crawler.Parse(ctx, c, parseIndex, indexResponses, "INDEX")
}

func articleCrawl(ctx context.Context, c *sync.WaitGroup,
	requests <-chan *crawler.Request) <-chan interface{} {
	articleResponses := crawler.Download(ctx, c, requests, "ARTICLES")
	return crawler.Parse(ctx, c, parseArticle, articleResponses, "ARTICLES")
}

func processIndexResults(ctx context.Context, c *sync.WaitGroup,
	rf *crawler.RequestsFactory, results <-chan interface{}, articleURLs chan *url.URL) {
	c.Add(1)
	go func() {
		defer c.Done()
		for {
			select {
			case <-ctx.Done():
				log.Printf("INDEX PROCESSOR: canceling - reason: %+v\n", ctx.Err())
				return
			case r, ok := <-results:
				if ok == false {
					log.Println("INDEX PROCESSOR: done")
					return
				}
				switch r.(type) {
				case articleURL:
					select {
					case articleURLs <- r.(articleURL):
					case <-ctx.Done():
					}
				case nextPageURL:
					requests := rf.RequestsFromSlide(ctx, "INDEX", r.(nextPageURL))
					go processIndexResults(ctx, c, rf, indexCrawl(ctx, c, requests), articleURLs)
				}
			}
		}
	}()
}

func main() {
	configFile := flag.String("config", "config.toml", "Path to the config file")
	collectionName := flag.String("collection", "politifact", "The collection where the items will be stored")

	flag.Parse()

	// DEBUG
	// go utils.DebugGR(time.Second * 10)

	ctx, stop := signal.NotifyContext(context.Background(), os.Interrupt, syscall.SIGTERM)
	defer stop()

	configD, err := ioutil.ReadFile(*configFile)
	if err != nil {
		log.Fatal(err)
	}
	config := config.Load(configD)

	mongoCli, err := mongoM.Connect(ctx, config.Mongo.URI)
	if err != nil {
		log.Fatal(err)
	}
	defer mongoM.Disconnect(ctx, mongoCli)

	db := mongoCli.Database(config.Mongo.Database)
	col := db.Collection(*collectionName)

	rf := &crawler.RequestsFactory{
		Method:  "GET",
		Header:  headers,
		Timeout: timeout,
	}

	// -- INDEX PAGE --
	startURL, err := url.Parse(indexURL)
	if err != nil {
		log.Fatal("Not a valid URL: ", indexURL)
	}

	startRequest := rf.RequestsFromSlide(ctx, "INDEX", startURL)

	articleURLs := make(chan *url.URL)
	var c1 sync.WaitGroup
	go processIndexResults(ctx, &c1, rf, indexCrawl(ctx, &c1, startRequest), articleURLs)

	var c2 sync.WaitGroup
	articleRequests := rf.RequestsFromStream(ctx, &c2, articleURLs, "ARTICLE")
	articleResults := articleCrawl(ctx, &c2, articleRequests)

	var pipelines sync.WaitGroup

	imp := importer.NewMongoImporter(col, nil)
	importer.Pipeline(ctx, &pipelines, imp, articleResults)

	c1.Wait()
	close(articleURLs)
	c2.Wait()
	pipelines.Wait()
}
