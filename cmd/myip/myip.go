package main

import (
	"bytes"
	"context"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"sync"
	"time"

	"gitlab.com/merfrei/ubur/crawler"
	"gitlab.com/merfrei/ubur/pkg/config"
	"gitlab.com/merfrei/ubur/pkg/pservice"
)

const websiteURL = "https://httpbin.org/ip"
const userAgent = "Mozilla/5.0 (X11; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0"

var headers = map[string]string{
	"User-Agent": userAgent,
}

var timeout = time.Duration(time.Second * 30)

func parseIndex(ctx context.Context, r *http.Response) <-chan interface{} {
	results := make(chan interface{})
	go func() {
		defer close(results)
		defer r.Body.Close()
		body, err := io.ReadAll(r.Body)
		if err != nil {
			log.Printf("PARSER [INDEX]: error found => %+v\n", err)
			return
		}
		select {
		case <-ctx.Done():
			log.Printf("PARSER [INDEX]: canceling - reason: %+v\n", ctx.Err())
			return
		case results <- fmt.Sprintf("%s\n", bytes.Replace(body, []byte("\n"), nil, -1)):
		}
	}()
	return results
}

func main() {
	configFile := flag.String("config", "config.toml", "Path to the config file")

	flag.Parse()

	configD, err := ioutil.ReadFile(*configFile)
	if err != nil {
		log.Fatal(err)
	}
	config := config.Load(configD)

	ctx, stopCrawl := context.WithCancel(context.Background())
	defer stopCrawl()

	ps := pservice.New(config.ProxyService.URL, config.ProxyService.APIKey, "example.com")
	err = ps.Start(ctx, make(map[string]string))
	if err != nil {
		log.Fatal(err)
	}

	var pipelines sync.WaitGroup

	requestsFactory := crawler.RequestsFactory{
		Method:       "GET",
		Header:       headers,
		Timeout:      timeout,
		ProxyService: ps,
	}

	// -- INDEX PAGE --
	startURL, err := url.Parse(websiteURL)
	if err != nil {
		log.Fatal("Not a valid URL: ", websiteURL)
	}

	startURLs := make([]*url.URL, 0)
	for i := 0; i < 100; i++ {
		startURLs = append(startURLs, startURL)
	}

	indexResponses := crawler.Download(
		ctx, &pipelines,
		requestsFactory.RequestsFromSlide(ctx, "INDEX", startURLs...), "INDEX")
	indexResults := crawler.Parse(ctx, &pipelines, parseIndex, indexResponses, "INDEX")

	pipelines.Add(1)
	go func() {
		defer pipelines.Done()
		for {
			select {
			case <-ctx.Done():
				log.Printf("RESULTS PROCESSOR: canceling - reason: %+v\n", ctx.Err())
				return
			case result, ok := <-indexResults:
				if !ok {
					log.Println("RESULTS PROCESSOR: done")
					return
				}
				fmt.Println(result)
			}
		}
	}()

	pipelines.Wait()
}
