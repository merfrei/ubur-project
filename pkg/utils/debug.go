package utils

import (
	"os"
	"runtime/pprof"
	"time"
)

// DebugGR will show the running goroutines and panic after a given time
func DebugGR(duration time.Duration) {
	timer := time.NewTimer(duration)
	<-timer.C
	pprof.Lookup("goroutine").WriteTo(os.Stdout, 1)
	panic("Timeout")
}
