// Package selectors contains utilities for selecting and parsing content
package selectors

import (
	"strings"

	"github.com/PuerkitoBio/goquery"
)

// FindAttr is a utility to return the attribute name for a child
func FindAttr(s *goquery.Selection, goquerySelector, attrName string) string {
	if attr, ok := s.Find(goquerySelector).Attr(attrName); ok {
		return strings.TrimSpace(attr)
	}
	return ""
}
